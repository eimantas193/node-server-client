var first=0;
var connections=0;

const net = require('net');
const util = require('util');
const setTimeoutPromise = util.promisify(setTimeout);
const server1 = net.createServer((a) =>{
    console.log('client on port 8125 connected');
    a.on('end', () => {
        console.log('client disconnected');
      });
    a.on('data', function(data) {
        console.log(data.toString());
        if (data=="Please\r\n")
        {
            console.log(data.toString());
            connections++;
        }
    });
    setTimeoutPromise(1000).then((value)=>{
        connections--;
        a.end();
  });
});


const server2 = net.createServer((b) =>
{   console.log('client on port 8126 connected');
b.on('data', function(data) {
    if (data=="Please\r\n")
    {
      console.log(data.toString());
      connections++;
    }
  });
  setTimeoutPromise(1000).then((value)=>{
    connections--;
    b.end();
  });
});

const server3 = net.createServer((c) =>
{   console.log('client on port 8127 connected');
c.on('data', function(data) {
    if (data=="Please\r\n")
    {
      console.log(data.toString());
      connections++;
      if (connections==3)
      {
        c.write('   The Cake is a lie!\n      $$  $$  $$\n    __||__||__||__\n   | * * * * * * *|\n   |* * * * * * * |\n   | * * * * * * *|\n   |______________|\n\r');
      }
      else
      { 
          c.write('You need to try harder! \r\n' )
        }
        c.pipe(c);
        c.end();
    }
    setTimeoutPromise(1000).then((value)=>{
        connections=0;
      });
  });
});

const server = net.createServer((d) => {
  // 'connection' listener.
  console.log('client on port 8124 connected');
  
  d.on('end', () => {
    console.log('client disconnected');
  });
  if (first==0)
  {
    d.write('1st stage completed. If you want cake say: "Please". \r\n');
    d.pipe(d);
    first=1;
  }
  d.on('data', function(data) {
      if (data=="Please\r\n")
      {
        console.log(data.toString());
        d.write('2st stage completed. If you really want it you would need try 3 times harder. Ports: 8125, 8126, 8127. \r\n');
        d.pipe(d);
        d.end();
      }
    });

});

server.on('error', (err) => {
  throw err;
});
server.listen(8124, () => {
    console.log('main_server bound');
  });
server1.listen(8125, () => {
    console.log('server1 bound');
});
server2.listen(8126, () => {
    console.log('server2 bound');
});
server3.listen(8127, () => {
   console.log('server3 bound');
});